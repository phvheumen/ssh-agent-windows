# SSH agent forwarding on Windows

## 0. Prerequisites
Download PuTTY from: https://www.chiark.greenend.org.uk/~sgtatham/putty/latest.html
And choose the windows installer. It should install all the required binaries for using SSH-keys and the SSH-agent (`Pageant`).

## 1. Generate a SSH key
Start `PuTTYgen`.
Generally SSH keys are of type `RSA` with 2048 bits. For better security one can choose a higher number of bits, but 2048 is likely to be sufficient for the years to come.
Click `Generate` and follow the instructions of the program. Once a key is generated it is highly recommended to protect it with a password.
Save both the private and the public key file to a convient place. To follow the same conventions as in Linux I suggest to save the keys in `%USERPROFILE%/.ssh` (in Linux the keys are typically store in `${HOME}/.ssh`)

## 2. Loading SSH key in Pageant
Start `Pageant` and use `Add Key (encrypted)` to load the just created key into the SSH agent. The key will be loaded and is still encrypted by the password that was recommended by the previous step. Go back to 1. if you didn't use a password to protect your SSH-key! The first time the SSH-agent requires to do something with your key, it will ask for the password.

![Key added to Pageant](img/01_add_key_pageant.PNG)

## 3a. Putty with SSH-agent forwarding
Please refer to [Getting started with PuTTY](https://the.earth.li/~sgtatham/putty/0.76/htmldoc/Chapter2.html) if you do not know how to start a SSH session in Putty.

To use agent-forwarding, navigate to `Connection -> SSH -> Auth` in configuration categories listed on the left. And check the `Allow agent forwarding` option. See image below.

![SSH-agent forwarding PuTTY](img/02_putty_ssh_agent_forwarding.PNG)

### Verify you agent is forwarded
When the SSH session is started with agent forwarding enabled, you can verify the forwarding actually work by listing the environment variables.
```
env | grep -i ssh
```
You should now see a variable `SSH_AUTH_SOCK` pointing to some random generated file in the `/tmp` directory. The actual file location depends on the OS and configuration of the SSH server daemon. But typically it is located in the `/tmp` directory.
```
ssh-add -L
```
Should also being able to list you SSH-key. See image below of example output

![Verify SSH-agent forwarding](img/03_putty_verify_ssh_agent_is_forwarded.PNG)

## 3b. MobaXterm with agent forwarding
If you don't know how to start a SSH session with MobaXterm, check out the [Documentation](https://mobaxterm.mobatek.net/documentation.html).

MobaXterm enables the usage of `Pageant` by default. To verify that go to `Settings -> Configuration -> SSH` and verify that `Use external Pageant` is checked. See image below.

To verify the agent forwarding is actually working, see the previous section.

![MobaXterm SSH agent forwarding](img/04_MobaXterm_SSH_agent_forwarding.PNG)

## 4. Load Pageant at boot (with keys loaded)
`Pageant` doesn't automatically start on boot. To enable that you can place a shortcut to `Pageant` in `%APPDATA%\Microsoft\Windows\Start Menu\Programs\Startup`. However, it will not load your keys by default. We can provide commandline arguments to the shortcut to load our keys. Open the properties of the just created shortcut. Assuming you have placed the keys in the suggested location from the first step, change the field `Start in:` to `%USERPROFILE%\.ssh\` and append to the `Target` field `--encrypted "id_rsa_putty.ppk"`. Ofcourse, use the keyname you have used in the first step. In my case that was `id_rsa_putty`

You shortcut settings should look like.

![Pageant Shortcut](img/06_Pageant_on_boot.PNG)
